package no.uib.inf101.gridview;

import no.uib.inf101.colorgrid.ColorGrid;

import no.uib.inf101.colorgrid.IColorGrid;
import no.uib.inf101.colorgrid.*;

import javax.swing.*;
import java.awt.*;

public class Main {
  public static void main(String[] args) {
    IColorGrid grid = new ColorGrid(3,4);
    //System.out.println(grid.rows());
    //System.out.println(grid.cols());

    //System.out.println(grid.get(new CellPosition(1, 2)));

    // Sjekk at vi kan endre verdien på en gitt posisjon
    grid.set(new CellPosition(0, 0), Color.RED);
    grid.set(new CellPosition(0, 3), Color.BLUE);
    grid.set(new CellPosition(2, 0), Color.YELLOW);
    grid.set(new CellPosition(2, 3), Color.GREEN);
    //System.out.println(grid.get(new CellPosition(1, 2))); // forventer rød
    //System.out.println(grid.get(new CellPosition(2, 1))); // forventer null

    GridView gridView = new GridView(grid);

      JFrame frame = new JFrame();
      frame.setTitle("INF101");
      frame.setContentPane(gridView);
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.pack();
      frame.setVisible(true);


  }
}
