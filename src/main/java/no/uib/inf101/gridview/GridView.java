package no.uib.inf101.gridview;

import no.uib.inf101.colorgrid.*;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.List;

public class GridView extends JPanel {
  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;

    IColorGrid colorGrid;
    public  GridView(IColorGrid colorGrid ){
      this.setPreferredSize(new Dimension(400, 300));
      this.colorGrid= colorGrid;

    }
    public void  drawGrid(Graphics2D g ) {
      Rectangle2D grid = new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN, getWidth()-OUTERMARGIN*2, getHeight()-OUTERMARGIN*2);
      g.setColor(MARGINCOLOR);
      g.fill(grid);
      CellPositionToPixelConverter cellPositionToPixelConverter= new CellPositionToPixelConverter(grid, colorGrid, OUTERMARGIN);
      drawCells(g,colorGrid,cellPositionToPixelConverter);
    }
   private static void  drawCells(Graphics2D g,CellColorCollection cellColor,CellPositionToPixelConverter cp) {
     List<CellColor> listColor=  cellColor.getCells();
     for (CellColor cell : listColor){
       Rectangle2D cell2D = cp.getBoundsForCell(cell.cellPosition());
       Color color= cell.color();
       if (cell.color()==null){
         color = MARGINCOLOR;}
       g.setColor(color);
       g.fill(cell2D);
     }



    }
    public void paintComponent(Graphics2D g){
      super.paintComponent(g);
      Graphics2D g2= (Graphics2D) g  ;
     drawGrid(g2);


   


    }
}
