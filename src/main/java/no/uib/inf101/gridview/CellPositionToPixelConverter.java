package no.uib.inf101.gridview;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

import java.awt.geom.Rectangle2D;

public class CellPositionToPixelConverter {
  Rectangle2D box ;
  double margin;
  GridDimension gd ;

  public CellPositionToPixelConverter(Rectangle2D box,GridDimension gd, double margin) {
    this.box =box;
    this.gd = gd;
    this.margin = margin;
  }

    public Rectangle2D getBoundsForCell( CellPosition cp){
       double cellWidth= (box.getWidth()-margin* (gd.cols()+1))/gd.cols();
       double cellHeight=(box.getHeight()-margin* (gd.rows()+1))/ gd.rows();
       double cellX= box.getX()+(margin* (cp.col()+1))+(cellWidth* cp.col());
       double cellY= box.getY()+(margin* (cp.row()+1))+(cellHeight* cp.row());;
       return new Rectangle2D.Double(cellX,cellY,cellWidth,cellHeight);

  }

}
