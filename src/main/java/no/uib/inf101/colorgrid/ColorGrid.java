package no.uib.inf101.colorgrid;

import com.ibm.icu.impl.Pair;

import java.awt.*;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.awt.Color;


public class ColorGrid implements IColorGrid{
  int rows;
  int cols;
  String color;
Hashtable<CellPosition,Color> grid= new Hashtable<>();
  List<CellColor> colorGrid= new ArrayList<>();
  List<CellPosition> gridList= new ArrayList<>();
 public ColorGrid (int rows,int cols){
   this.rows=rows;
   this.cols= cols;
   for(int i = 1; i== rows; i++){
     for(int j = 1; j==cols; j++){
       grid.put(new CellPosition(i,j),Color.white);
       gridList.add(new CellPosition(i,j));}
   }
 }
  @Override
  public List<CellColor> getCells() {
    return colorGrid ;
  }

  @Override
  public int rows() {
    return this.rows;
  }

  @Override
  public int cols() {
    return this.cols;
  }

  @Override

  public Color get(CellPosition pos) {
   if (pos.row()<0 || pos.row()>this.rows -1|| pos.col()<0 || pos.col()>this.cols-1){
     throw new IndexOutOfBoundsException(" in");
   }
    for (CellColor cel:colorGrid
         ) {
      if(cel.cellPosition().equals(pos)) {
        return cel.color();
      }
    }
    return null;

  }

  @Override
  public void set(CellPosition pos, Color color) {
    if (pos.row()<0 || pos.row()>this.rows -1|| pos.col()<0 || pos.col()>this.cols-1){
      throw new IndexOutOfBoundsException(" The position is out of boundary(col or row)");
    }
   colorGrid.add(new CellColor(pos,color));
//     grid.put(pos,color);

  }

}
